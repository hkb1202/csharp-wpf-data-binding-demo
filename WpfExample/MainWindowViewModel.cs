﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace WpfExample
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        //实现接口当数据源变动通知前台UI
        public event PropertyChangedEventHandler? PropertyChanged;
        public void RaisePropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

        /// <summary>
        /// 前台DataGrid绑定的People集合
        /// </summary>
        public ObservableCollection<Person> People { get; set; }

        /// <summary>
        /// 绑定前台DataGrid控件SelectedItem字段上，用于保存当前选中的Item所对应的数据源
        /// </summary>
        public Person SelectItem
        {
            get
            {
                return m_SelectItem;
            }
            set
            {
                m_SelectItem = value;
                RaisePropertyChanged("SelectItem");
            }
        }

        /// <summary>
        /// DataGrid控件中删除按钮命令
        /// </summary>
        public Command DelClick
        {
            get
            {
                if (m_DelClick == null)
                    m_DelClick = new Command(DeleteEvent);

                return m_DelClick;
            }
        }

        /// <summary>
        /// 前台添加小刚按钮命令
        /// </summary>
        public Command AddClick
        {
            get
            {
                if (m_AddClick == null)
                    m_AddClick = new Command(AdditionEvent);

                return m_AddClick;
            }
        }

        /// <summary>
        /// 前台修改Text按钮命令
        /// </summary>
        public Command ReviseClick
        {
            get
            {
                if (m_ReviseClick == null)
                    m_ReviseClick = new Command(ReviseEvent);

                return m_ReviseClick;
            }
        }

        /// <summary>
        /// 前台TextBlock控件显示的文本
        /// </summary>
        public string TextInfo
        {
            get
            {
                return m_TextInfo;
            }
            set
            {
                m_TextInfo = value;
                //数据源更新调用更新前台UI方法
                RaisePropertyChanged("TextInfo");
            }
        }

        /// <summary>
        /// DataGrid控件电话信息的TextBox键盘按下回车命令
        /// </summary>
        public Command PressEnterKey
        {
            get
            {
                if (m_PressEnterKey == null)
                    m_PressEnterKey = new Command(PressEnterKeyEvent);

                return m_PressEnterKey;
            }
        }

        private Person m_SelectItem;
        private Command m_DelClick;
        private Command m_AddClick;
        private Command m_ReviseClick;
        private string m_TextInfo;
        private Command m_PressEnterKey;

        /// <summary>
        /// 构造方法
        /// </summary>
        public MainWindowViewModel()
        {
            People = new ObservableCollection<Person>();

            Person person1 = new Person() { Name = "小明", Age = 12, Sex = "男", Phone = "110" };
            Person person2 = new Person() { Name = "小红", Age = 13, Sex = "女", Phone = "119" };
            Person person3 = new Person() { Name = "小王", Age = 15, Sex = "男", Phone = "120" };

            People.Add(person1);
            People.Add(person2);
            People.Add(person3);

            TextInfo = "点击右侧按钮这里内容将会变化！";
        }

        /// <summary>
        /// DataGrid控件中删除按钮事件
        /// </summary>
        /// <param name="obj">可传入前台控件</param>
        private void DeleteEvent(object obj)
        {
            if (MessageBox.Show($"是否删除{SelectItem.Name}的数据？", "提示", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                People.Remove(SelectItem);
            }
        }

        /// <summary>
        /// 前台添加小刚按钮事件
        /// </summary>
        /// <param name="obj">可传入前台控件</param>
        private void AdditionEvent(object obj)
        {
            if (MessageBox.Show("是否添加“姓名：小刚，年龄：18，性别：女，电话：123”？", "提示", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                People.Add(new Person() { Name = "小刚", Age = 18, Sex = "女", Phone = "123" });
            }
        }

        /// <summary>
        /// 前台修改Text按钮事件
        /// </summary>
        /// <param name="obj">可传入前台控件</param>
        private void ReviseEvent(object obj)
        {
            if (TextInfo == "点击右侧按钮这里内容将会变化！")
            {
                TextInfo = "点击了右侧按钮!!!!!!!!！";
            }
            else
            {
                TextInfo = "点击右侧按钮这里内容将会变化！";
            }
        }

        /// <summary>
        /// DataGrid控件电话信息的TextBox键盘按下回车事件
        /// </summary>
        /// <param name="obj">可传入前台控件</param>
        private void PressEnterKeyEvent(object obj)
        {
            TextBox textBox = (TextBox) obj;
            MessageBox.Show($"点击了回车！控件内容为：{textBox.Text}");
        }

        /// <summary>
        /// 数据结构
        /// </summary>
        public class Person
        {
            public string Name { get; set; }
            public int Age { get; set; }
            public string Sex { get; set; }
            public string Phone { get; set; }
        }
    }
}
